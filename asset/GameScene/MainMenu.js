Game.MainMenu = function(game){

}

var data = {snakeLength:0};
let input = {x:0, isDown:false};
let time = 0;
let timeTap = 0;
let snk = null;

Game.MainMenu.prototype = {
	snake : {
		head : null,
		body : null,
		position : null,
		lastPos : null,
		length : 0,
		yChange : 0,
		yStart : 0
		
	},
    create : function(){
		
		this.physics.startSystem(Phaser.Physics.ARCADE);
		
		//this.physics.arcade.gravity.y = 0;
		
		
		let canvas = this.add.sprite(0,0, 'Canvas');
		canvas.height = 1280;
		canvas.width = 720;
		
		this.physics.enable(canvas,Phaser.Physics.ARCADE);
		canvas.body.setCircle(100, 0,0);
		canvas.body.damping = 0.8;
		canvas.body.static = true;
		
		this.input.addMoveCallback(this.onDrag , this);
		
		this.snake.body = new Array();
		this.snake.position = new Array();
		
		for(let i = 0; i  < 200; i++){
			this.snake.position[i] = {x:400,y:1200,setTo:function(a,b){this.x=a; this.y=b;}};
		};
		
		this.snake.head = this.createRope(this.snake.tail, 400, 1200);
		this.snake.lastPos = {x:400 , y:1200,setTo:function(a,b){this.x=a; this.y=b;}};
		this.snake.length = 1;
		this.snake.body[0] = this.snake.head;
		
		
		this.camera.follow(this.snake.head , Phaser.Camera.FOLLOW_LOCKON, 0.5, 0.5);
		this.camera.deadzone = new Phaser.Rectangle(0, 750, 720, 200);
		console.log(data.snakeLength);
		console.log('Main Menu : Welcome in Game Snake');
	
    },
	update : function(){
		this.gameLoop(this.time.elapsed/1000);
	},
	gameLoop : function(delta){
		
		this.world.setBounds(0, -this.snake.yChange , 720, 1280 + this.snake.yChange);
		
		if(this.snake.length < data.snakeLength){
			time += delta;
			if(time > 0.1){
				//console.log("Create")
				this.snake.body[this.snake.length] = this.createRope(this.snake.body[this.snake.length-1], this.snake.body[this.snake.length-1].x, this.snake.body[this.snake.length-1].y);
				this.snake.length++;
				time = 0;
			}
		}
		this.snake.head.body.velocity.y = -300;
		//this.snake.head.body.velocity.x = -500;
		
		this.snake.yChange = Math.max(this.snake.yChange , Math.abs(this.snake.head.y - this.snake.yStart));

		this.addSnakeMove();
		
		let i = 1;
		while(this.snake.body[i] && i*6-1 < this.snake.position.length){
			this.snake.body[i].x = this.snake.position[i*6-1].x;
			this.snake.body[i].y = this.snake.position[i*6-1].y;
			i++;
		}
		
		
		
		
		
		//console.log(delta);
		
	},
	addSnakeMove : function(){
		
		let pos = {x:0,y:0};
		pos.x = (this.snake.head.x-this.snake.lastPos.x);
		pos.y = (this.snake.head.y-this.snake.lastPos.y);
		let vectorLength = Math.sqrt( Math.pow(pos.x,2)+Math.pow(pos.y,2) );
		
		if(vectorLength > 5 && Math.abs(pos.x) > 0.1){
			
			pos.x = pos.x/vectorLength;
			pos.y = pos.y/vectorLength;
			let part = null;
			
			
			let loop = Math.floor(vectorLength/5);
			
			for(let i = 0; i < loop; i++){		
				part = this.snake.position.pop();
				part.setTo(this.snake.position[0].x + pos.x*5, this.snake.position[0].y + pos.y*5);
				this.snake.position.unshift(part);
			}
			
			this.snake.lastPos.setTo(this.snake.position[0].x , this.snake.position[0].y);
			
		}else{
			
			let part = this.snake.position.pop();
			part.setTo(this.snake.head.x, this.snake.head.y);
			this.snake.position.unshift(part);
			this.snake.lastPos.setTo(this.snake.head.x, this.snake.head.y);
		}
	
	},
	onDrag : function(pointer , x , y) {
        
		if(pointer.isDown){
			timeTap = 0;
			if(!input.isDownFirst){
				input.isDownFirst = true;
				input.x = x;
				//console.log("Point : ("+x+" , "+y+")");
				return;
			}
			let movement = Math.clamp((x-input.x) , -80 , 80);
			
			this.snake.head.x += movement ;
			
			input.x = x;
		}else if(pointer.isUp){
			input.isDownFirst = false;
		}


        
    },
	createRope:function(lastRect , xAnchor, yAnchor) {
		let radius = 20;

		newRect = this.add.sprite(xAnchor, yAnchor, 'Ship', 0);
		
		
		if (lastRect)
		{
			//newRect.anchor.setTo(0.5,0.5);
		}else{
			this.snake.yStart = yAnchor;
			
			this.physics.enable(newRect, Phaser.Physics.ARCADE);
			newRect.body.setCircle(radius, 0,0);
			//newRect.body.damping = 0;
			newRect.body.bounce.set(0);
			newRect.body.collideWorldBounds = true;
		}
		
		return newRect;
	}
}


